<div align="center">
  <h1>Proxy</h1>
</div>

<div align="center">
  <img src="proxy_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Proxy is a structural pattern that is a surrogate for another object and controls access to it.
**

### Real-World Analogy

_A credit card._

Instead of paying with a bundle of cash (analogy for service) we can use a debit card (proxy).
Through the debit card and pin code, access to the money is controlled.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

Proxy (ImageProxy)-maintains a reference that lets the proxy access the real subject. Proxy may refer to a Subject if
the RealSubject and Subject interfaces are the same.-provides an interface identical to Subject's so that a proxy can by
substituted for the real subject.-controls access to the real subject and may be responsible for creating and deleting
it.210 STRUCTURAL PATTERNS CHAPTER 4- other responsibilities depend on the kind of proxy: • remote proxies are
responsible for encoding a request and its arguments and for sending the encoded request to the real subject in a
different address space. • virtual proxies may cache additional information about the real subject so that they can
postpone accessing it. For example, the ImageProxy from the Motivation caches the real image's extent. • protection
proxies check that the caller has the access permissions required to perform a request. • Subject (Graphic)- defines the
common interface for RealSubject and Proxy so that a Proxy can be used anywhere a RealSubject is expected. •
RealSubject (Image)- defines the real object that the proxy represents.

### Collaborations

...
Proxy forwards requests to RealSubject when appropriate, depending on the
kind of proxy.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

Proxy is applicable whenever there is a need for a more versatile or sophisticated reference to an object than a simple
pointer. Here are several common situations in which the Proxy pattern is applicable: 1. A remote proxy provides a local
representative for an object in a different address space. NEXTSTEP [Add94] uses the class NXProxy for this purpose.
Coplien [Cop92] calls this kind of proxy an "Ambassador." 2. A virtual proxy creates expensive objects on demand. The
ImageProxy described in the Motivation is an example of such a proxy. 3. A protection proxy controls access to the
original object. Protection proxies are useful when objects should have different access rights. For example,PROXY 209
KernelProxies in the Choices operating system [CIRM93] provide protected access to operating system objects. 4. A smart
reference is a replacement for a bare pointer that performs additional actions when an object is accessed. Typical uses
include • counting the number of references to the real object so that it can be freed automatically when there are no
more references (also called smart pointers [Ede92]). • loading a persistent object into memory when it's first
referenced. • checking that the real object is locked before it's accessed to ensure that no other object can change it.

### Motivation

- ...

One reason for controlling accessto an object is to defer the full cost of its creation
and initialization until we actually need to use it. Consider a document editor
that can embed graphical objectsin a document. Some graphical objects, like large
raster images, can be expensive to create.But opening a document should be fast,
sowe should avoid creating all the expensive objects at once when the document
is opened. This isn't necessary anyway, because not all of these objects will be
visible in the document at the same time.

These constraints would suggest creating each expensive object ondemand, which
in this case occurs when an image becomes visible. But what do we put in the
document in place of the image? And how can we hide the fact that the image is
created on demand sothat we don't complicatethe editor'simplementation?This
optimization shouldn't impact the rendering and formatting code, for example.

The solution is to use another object, an image proxy, that acts as a stand-in for
the real image. The proxy actsjust like the image and takes care of instantiating it
when it's required.

The image proxy creates the real image only when the document editor asks it
to display itself by invoking its Draw operation. The proxy forwards subsequent
requests directly to the image.It must therefore keep a referenceto the image after
creating it.

Let's assume that images are stored in separate files. In this case we can use the
file name as the reference to the real object. The proxy also stores its extent, that
is, its width and height. The extent lets the proxy respond to requests for its size
from the formatter without actually instantiating the image.
The following class diagram illustrates this example in more detail.

The document editor accesses embedded images through the interface defined by
the abstract Graphic class. ImageProxy is a class for images that are created on
demand. ImageProxy maintains the file name as a reference to the image on disk.
The file name is passed as an argument to the ImageProxyconstructor.

ImageProxy also stores the bounding box of the image and a reference to the
real Image instance. This reference won't be valid until the proxy instantiates
the real image. The Draw operation makes sure the image is instantiated before
forwarding it the request. GetExtentforwardsthe request to the image only if it's
instantiated; otherwise ImageProxy returns the extent it stores.

---

There are dozens of ways to utilize the Proxy pattern. Let’s go over the most popular uses.

Lazy initialization (virtual proxy). This is when you have a heavyweight service object that wastes system resources by
being always up, even though you only need it from time to time.

Instead of creating the object when the app launches, you can delay the object’s initialization to a time when it’s
really needed.

Access control (protection proxy). This is when you want only specific clients to be able to use the service object; for
instance, when your objects are crucial parts of an operating system and clients are various launched applications (
including malicious ones).

The proxy can pass the request to the service object only if the client’s credentials match some criteria.

Local execution of a remote service (remote proxy). This is when the service object is located on a remote server.

In this case, the proxy passes the client request over the network, handling all of the nasty details of working with
the network.

Logging requests (logging proxy). This is when you want to keep a history of requests to the service object.

The proxy can log each request before passing it to the service.

Caching request results (caching proxy). This is when you need to cache results of client requests and manage the life
cycle of this cache, especially if results are quite large.

The proxy can implement caching for recurring requests that always yield the same results. The proxy may use the
parameters of requests as the cache keys.

Smart reference. This is when you need to be able to dismiss a heavyweight object once there are no clients that use it.

The proxy can keep track of clients that obtained a reference to the service object or its results. From time to time,
the proxy may go over the clients and check whether they are still active. If the client list gets empty, the proxy
might dismiss the service object and free the underlying system resources.

The proxy can also track whether the client had modified the service object. Then the unchanged objects may be reused by
other clients.

### Known Uses

- ...

Remote Proxy: Acts as a local representative for an object that resides in a different address space, like a remote
server. This proxy manages the communication between the local and remote objects.

Virtual Proxy: Creates placeholders for large or resource-intensive objects until they are needed. For example, loading
images or complex data structures lazily rather than immediately to improve performance.

Protection Proxy: Controls access to sensitive objects by implementing access control mechanisms. This can involve
checking permissions or validating requests before allowing access to the real object.

Logging Proxy: Records information such as method calls, access times, or other metrics before passing requests to the
real object. This is useful for debugging, monitoring, or profiling applications.

Cache Proxy: Stores the results of expensive operations and returns cached results when the same operation is requested
again. It enhances performance by reducing the number of calls to the real object.

Smart Reference Proxy: Performs additional actions when an object is referenced, like counting the number of references
to the real object or loading it into memory when it's first accessed.

Synchronization Proxy: Controls access to shared resources by synchronizing multiple requests to the real object to
ensure thread safety and prevent concurrent access issues.

Consider a file system where certain files should only be accessible to users with specific privileges. The Protection
Proxy acts as a guard, checking the user's credentials or permissions before allowing access to those sensitive files.
It ensures that only authorized users can interact with the protected object, while unauthorized users are denied
access.

In a web application, a Caching Proxy can be used to store frequently accessed web pages or resources locally. When a
user requests a web page, the proxy first checks if it's in the cache. If it is, the cached page is served, reducing the
need to fetch the page from the server. This speeds up response times and reduces server load.

Lazy Initialization Proxy: Delays the creation of an object until it is actually needed, optimizing resource
utilization. For instance, in a database application, the Proxy might delay establishing a database connection until a
query is made.

Mock Proxy: In testing environments, a Proxy can simulate the behavior of a real object to perform tests without
involving the actual implementation. This helps in isolating components for testing purposes.

Firewall Proxy: Controls and filters requests to resources based on predefined rules. It can be used to protect
sensitive resources by allowing or denying access based on certain criteria.

Throttling Proxy: Manages the frequency of requests sent to a resource, ensuring that it doesn't get overwhelmed. For
example, limiting the number of API requests made to a service within a specific time frame.

Caching Proxy with Invalidation: Implements a caching mechanism but includes methods to invalidate or refresh the cache
based on certain events or changes in the underlying data.

Audit Proxy: Records and logs all interactions with an object for auditing purposes, maintaining a history of actions
performed on the real object.

Encryption Proxy: Encrypts data before passing it to the real object and decrypts the data when it returns, providing an
additional layer of security.

Load Balancing Proxy: Distributes incoming requests across multiple instances of an object or service, helping to evenly
distribute the workload and improve overall system performance.

java.lang.reflect.Proxy
java.rmi.*
javax.ejb.EJB (explanation here)
javax.inject.Inject (explanation here)
javax.persistence.PersistenceContext

The virtual proxy example in the Motivation section is from the ET++ text building
block classes.
NEXTSTEP [Add94] uses proxies (instances of class NXProxy) as local represen-
tatives for objects that may be distributed. A server creates proxies for remote
objects when clients request them. On receiving a message, the proxy encodes it
along with its arguments and then forwards the encoded message to the remote
subject. Similarly, the subject encodes any return results and sends them back to
the NXProxy object.
McCullough [McC87] discusses using proxies in Smalltalk to access remote ob-
jects. Pascoe [Pas86] describes how to provide side-effects on method calls and
access control with "Encapsulators.


**remote proxy:** A remote proxy acts as a local representative to a remote object.

- a remote object that is running in a different address space
- “local representative” is an object that you can call local methods on and have them forwarded on to the remote object

**Virtual Proxy:** Virtual Proxy acts as a representative for an object that may be expensive to create.

- The Virtual Proxy often defers the creation of the object until it is needed;
- the Virtual Proxy also acts as a surrogate for the object before and while it is being created. After that, the proxy
  delegates requests directly to the RealSubject.

**Firewall proxy:** controls access to a set of network resources, protecting the subject from "bad" cleints
Smart reference proxy: provides additional actions whenever a subject is referenced, such as counting the number of
references to an object
**Caching proxy:** provides temporary storage for results of operations that are expensive. It can also allow multiple
clients to share the results to reduce computation or network latency
**Synchronization proxy:** provides safe access to a subject from multiple threads
**Complexity hiding proxy:** hides the complexity of and controls access to a complex set of classes. This is sometimes
called the facade proxy for obvious reasons. The complexity hiding proxy differs from the facade pattern in that the
proxy controls access, while the facade pattern just provides an alternative interface.
**Copy on write proxy:** Controls the copying of an object by deferring the copying of an object until it is required by
a client. This is a variant of the virtual proxy.

### Categorization

Purpose:  **Structural**  
Scope:    **Object**   
Mechanisms: **Polymorphism**

Structural patterns are concerned with how classes and objects are composed to form larger structures.
Structural class patterns use inheritance to compose interfaces or implementations.
Structural object patterns describe ways to compose objects to realize new functionality.

### Aspects that can vary

- How an object is accessed; its location.

### Solution to causes of redesign

- Dependence on object representations or implementations.
    - Clients that know how an object is represented, stored, located, or implemented might need to be changed when the
      object changes.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

The Proxy pattern introduces a level of indirection when accessing an object. The
additional indirection has many uses, depending on the kind of proxy:

1. A remote proxy can hide the fact that an object resides in a different address
   space.
2. A virtual proxy can perform optimizations such as creating an object on
   demand.
3. Bothprotection proxies and smart references allow additional housekeeping
   tasks when an object is accessed.
   There's another optimization that the Proxy pattern can hide from the client. It's
   called copy-on-write,and it's related to creation on demand. Copying a large and
   complicated object can be an expensive operation. If the copy is never modified,
   then there's no need to incur this cost. Byusing a proxy to postpone the copying
   process, we ensure that we pay the price of copying the object only ifit's modified.
   To make copy-on-write work, the subjectmust be reference counted. Copying the
   proxy will do nothing more than increment this reference count. Only when the
   client requests an operation that modifiesthe subjectdoes the proxy actuallycopy
   it. In that case the proxy must also decrement the subject's reference count. When
   the reference count goes to zero, the subject gets deleted. Copy-on-write can reduce the cost of copying heavyweight
   subjects significantly.

You can control the service object without clients knowing about it.
You can manage the lifecycle of the service object when clients don’t care about it.
The proxy works even if the service object isn’t ready or is not available.
Open/Closed Principle. You can introduce new proxies without changing the service or clients.

The code may become more complicated since you need to introduce a lot of new classes.
The response from the service might get delayed.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

Adapter (139): An adapter provides a different interface to the object it adapts.
In contrast, a proxy provides the same interface as its subject.However, a proxy
used for access protection might refuse to perform an operation that the subject
will perform, so its interface may be effectively a subset of the subject's.
Decorator (175): Although decorators can have similar implementations as proxies, decorators have a different purpose.
A decorator adds one or more responsibilities to an object, whereas a proxy controls access to an object.
Proxies vary in the degree to which they are implemented like a decorator. A
protection proxy might be implemented exactly like a decorator. On the other
hand, a remote proxy will not contain a direct reference to itsreal subject but only
an indirect reference, such as "host IDand local address on host." A virtual proxy
will start off with an indirect reference such as a file name but will eventually
obtain and use a direct reference.

How do I make clients use the Proxy rather than the Real Subject?
One common technique is to provide a factory that instantiates and returns the subject.
we can then wrap the subject with a proxy before returning it. The client never knows or cares that it’s using a proxy
instead of the real thing

ImageProxy seems just like a Decorator to me. What is the difference?
their purposes are different: a decorator adds behavior to a class, while a proxy controls access to it.

An adapter seems very similar as well. What is the difference
Adapter changes the interface of the objects it adapts, while the Proxy implements the same interface


<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

Proxies can be implemented by a class that delegate all of the real work to some other object. Each proxy method should,
in the end, refer to a service object unless the proxy is a subclass of a service.

(recognizeable by creational methods which returns an implementation of given abstract/interface type which in turn
delegates/uses a different implementation of given abstract/interface type)

### Structure

```mermaid
classDiagram
    class Subject {
        <<interface>>
        + request(): void
    }

    class RealSubject {
        + request(): void
    }

    class Proxy {
        - realSubject: RealSubject
        + request(): void
    }

    Subject <|.. RealSubject: implements
    Subject <|.. Proxy: implements
    Proxy o-- Subject: aggregated by
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

The Proxy pattern can exploit the following language features:

1. Overloading the member access operator in C++. C++ supports overloading
   operator->, the member access operator. Overloading this operator lets
   you perform additional work whenever an object is dereferenced. This can
   be helpful for implementing some kinds of proxy; the proxy behaves just like
   a pointer.
   The following example illustrates how to use this technique to implement a
   virtual proxy called ImagePtr.

```text
class Image;
extern Image* LoadAnlmageFile(const char*);
// external function
class ImagePtr {
public:
ImagePtr(const char* imageFile);
virtual "ImagePtr();
virtual Image* operator->();
virtual Image& operator*();
private:
Image* Loadlmage();
private:
Image * _image;
const char* _imageFile;
};
ImagePtr::ImagePtr (const char* thelmageFile) {
_imageFile = thelmageFile;
_image = 0;
}
Image* ImagePtr::Loadlmage () {
if (_image = = 0 ) {
_image = LoadAnlmageFile(_imageFile);
}
return _image;
```

The overloaded -> and * operators use Loadlmage to return _image to
callers (loading it if necessary)

```text
Image* ImagePtr::operator-> () {
return Loadlmage();
}
Image& ImagePtr::operator* () {
return *LoadImage();
}
```

This approach lets you call Image operations through ImagePtr objects
without going to the trouble of making the operations part of the ImagePtr
interface:

```text
ImagePtr image = ImagePtr("anlmageFileName");
image->Draw(Point(50, 100));
// (image.operator->())->Draw(Point(50, 100))
```

notice how the image proxy acts like a pointer, but it's not declared to be a
pointer to an Image. That means you can't use it exactly like a real pointer to
an Image. Hence clients must treat Image and ImagePtr objects differently
in this approach.
Overloading the member access operator isn't a good solution for every kind
of proxy. Some proxies need to know precisely which operation is called, and
overloading the member access operator doesn't work in those cases.
Consider the virtual proxy example in the Motivation. The image should
be loaded at a specific time—namely when the Draw operation is called—
and not whenever the image is referenced. Overloading the access operator
doesn't allow this distinction. In that case we must manually implement each
proxy operation that forwards the request to the subject.
These operations are usually very similar to each other, as the Sample Code
demonstrates. Typically all operations verify that the request is legal, that
the original object exists, etc., before forwarding the request to the subject.
It's tedious to write this code again and again. So it's common to use a
preprocessor to generate it automatically.

2. Using doesNotUnderstand in Smalltalk. Smalltalk provides a hook that
   you can use to support automatic forwarding of requests. Smalltalk calls
   doesNotUnderstand: aMessage when a client sends a message to a
   receiver that has no corresponding method. The Proxy class can redefine
   doesNotUnderstand so that the message is forwarded to its subject.
   To ensure that a request is forwarded to the subject and not just absorbed
   by the proxy silently, you can define a Proxy class that doesn't understand
   any messages. Smalltalk lets you do this by defining Proxy as a class with no
   superclass.6
   The main disadvantage of doesNotUnderstand: is that most Smalltalk
   systems have a few special messages that are handled directly by the virtual
   machine, and these do not cause the usual method look-up. The only one
   that's usually implemented in Object (and so can affect proxies) is the identity
   operation ==.
   If you're going to use doesNotUnderstand: to implement Proxy, then
   you must design around this problem. You can't expect identity on prox-
   ies to mean identity on their real subjects. An added disadvantage is that doesNotUnderstand: was developed for error
   handling, not for building
   proxies, and so it's generally not very fast.
3. Proxy doesn't always have to know the type of real subject. If a Proxy class can
   deal with its subject solely through an abstract interface, then there's no
   need to make a Proxy class for each RealSubject class; the proxy can deal
   with all RealSubject classes uniformly. But if Proxies are going to instantiate
   RealSubjects (such as in a virtual proxy), then they have to know the concrete
   class.
   Another implementation issue involves how to refer to the subject before it's
   instantiated. Some proxies have to refer to their subject whether it's on disk or
   in memory. That means they must use some form of address space-independent
   object identifiers. We used a file name for this purpose in the Motivation

### Implementation

In the example we apply the proxy pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Proxy](https://refactoring.guru/design-patterns/proxy)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Milan Jovanovic: Extremely FAST Caching Repository With Decorator Pattern in ASP.NET Core](https://youtu.be/i_3I6XLAOt0?si=Oj5LFxXehrNGQYbI)
- [Milan Jovanovic: How to Add Caching To Your Repository With the Decorator Pattern](https://youtu.be/ptl2-4adyCE?si=-V_PF3NJeZ3pmHRA)
- [Milan Jovanovic: How To Build a Load Balancer In .NET With YARP Reverse Proxy](https://youtu.be/0RaH9hhOF4g?si=MZTwobpfIMzDKdqe)
- [Geekific: The Proxy Pattern Explained and Implemented in Java](https://youtu.be/TS5i-uPXLs8?si=gQL9Frp_mXFPZLtu)

<br>
<br>
